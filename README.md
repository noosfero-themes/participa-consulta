Consulta Template theme for Participa.br/Noosfero
=================================================

This theme is a kind of template theme to be use on community type equal "Consulta" on Participa.br context
Layout type: Top and left bar
The covered areas/blocks are:
* Link-list (top and left)
* Members
* Statistics
* Article-block (main)
* Breadcrumbs

Install on /public/designs/themes/consulta-theme

