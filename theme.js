(function($){
  function setBreadcrumbs(){
    $('<span id="breadcrumbs-you-are-here">Você está aqui: </span>').insertBefore($('.breadcrumbs-plugin_content-breadcrumbs-block .block-inner-2').children().first());
  }

  $(document).ready(function(){
    setBreadcrumbs();
  });
})(jQuery);
